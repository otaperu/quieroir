new Controller.Presenter "nota",
  list:
    notas: -> Nota.find for: @_id
  new:
    events:
      "click button": (e, tpl) ->
        data = $("#notaNew").toObject()
        _.extend data,
          punto: if $(e.target).hasClass("up") then 1 else -1
          for: @_id
          at: Date.now()
        Usuario.current.addNota data
        Ruta.update @_id, $inc: punto: data.punto
        return false
