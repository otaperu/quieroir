class @locRuta extends BasicModel
  @collection "locruta", {connection:null}
  @hasOne "Detalle", "Recorrido", "rutaId"
class @Detalle extends BasicModel
  @collection "detalle", {connection:null}
mapLoaded = false
pos1 = undefined
pos2 = undefined

new Controller.Presenter "app",
  index:
    rendered: ->
      unless mapLoaded
        initMap(initialize)
        setMap()
        mapLoaded = true
    destroyed: -> mapLoaded = false
    events:
      "click .barra button": (e, tpl) ->
        e.preventDefault()
        e.stopPropagation()
        parent = $(e.target).parents(".barra")
        loc = parent.find("input").val()
        zip = parent.find("select").val()
        if loc
          marker = if parent.hasClass("barraIni") then markerA else markerB
          Meteor.call "getLocation", loc, zip, (err, res) ->
            unless err
              data = XML.parseXML(res.content).xmlresponse
              point = new google.maps.LatLng data.fcy0, data.fcx0
              marker.setPosition point
              updRoutes()
        else updRoutes()
      "click .favs": -> View.set("modalView","appFavs"); return false
      "click .help": -> View.set("modalView","appHelp"); return false
      "click .user": -> View.set("modalView","appUser"); return false
      "click .share": -> View.set("modalView","appShare"); return false
  indexRadius:
    events:
      "change select": (e, tpl) ->
        parent = $(e.target).parents(".barra")
        radius = parseInt e.target.value
        circle = if parent.hasClass("barraIni") then circleA else circleB
        circle.setRadius radius
  sidebar:
    rendered: ->
      $('.sidebar').enscroll
        minScrollbarLength: 28
        showOnHover: true
        verticalHandleClass: 'handle4'
        verticalTrackClass: 'track4'
    hasData: -> locRuta.count() or Detalle.count()
    recorridos: -> locRuta.find()
    rutaSeleccionada: -> Detalle.findOne({rutaId:Session.get("rutaClicked")})
    events:
      "click .showRuta": (e, tpl) ->
        a = markerA.position.lng()+" "+markerA.position.lat()
        b = markerB.position.lng()+" "+markerB.position.lat()
        Session.set("rutaClicked", @_id)
        unless @recorrido
          if @ruta_nro then Meteor.call "showRuta", @ruta_nro, a, b, showRecorrido
          else Meteor.call "showTransbordo", @rna, @rnb, a, b, showRecorrido
  sidebarRecorrido:
    rendered: -> $('.sidebar').scrollTo($('.panel-heading'), 800, {offset:-10})
    fixed: (nro, dec) -> dec = 2 unless _.isNumber dec; parseFloat(nro).toFixed(dec)
    events:
      "click a": (e, tpl) ->
        View.set "modalView",
          template: "appRuta"
          data:
            name: e.target.text
            ruta: Ruta.findOne( name:e.target.text )
  favs:
    favs: -> Favorito.find()
    events:
      "click .play": -> 
        markerA.setPosition new google.maps.LatLng(@lat, @lng)
        updRoutes()
      "click .stop": ->
        markerB.setPosition new google.maps.LatLng(@lat, @lng)
        updRoutes()
      "click .trash": -> @destroy()
  share:
    url: ->
      location.origin+"?start="+markerA.getPosition().toUrlValue()+"&end="+markerB.getPosition().toUrlValue()
  ruta:
    rendered: ->
      $(".rutaThumb").imagesLoaded()
      .progress (instance, image) ->
        # TODO: Añadir loader y notFound [thumb,avatar]
        image.img.style.display = "none" unless image.isLoaded
    empresa: -> @getEmpresa()
  modalView:
    events:
      "click .cancel": -> View.set "modalView", false
  user:
    events:
      "click .logout": (e, tpl) ->
        bootbox.confirm "¿Deseas salir?", (result) -> Meteor.logout() if result
  userSignin:
    events:
      "click .signin": (e, tpl) ->
        e.preventDefault()
        user = tpl.find(".user").value
        pass = tpl.find(".pass").value
        if user and pass
          Meteor.loginWithPassword user, pass, (err, res) ->
            View.set "modalView", false
        else console.log user, pass
      "click .signup": -> View.set("modalView","appUserSignup"); return false
  userSignup:
    events:
      "click .signup": (e, tpl) ->
        e.preventDefault()
        data = $("#signup").toObject()
        _.extend data, profile:{}
        Accounts.createUser form, (err) ->
          if err then alert(err)
          else View.set "modalView", false
      "click .signin": -> View.set("modalView","appUser"); return false

initMap = (done) ->
  pos1 = "-12.0633,-77.0365"
  pos2 = (getQuery("end") or "-12.120649,-77.02961").split(",")

  if navigator.geolocation
    navigator.geolocation.getCurrentPosition ((pos) ->
      pos1 = pos.coords.latitude + "," + pos.coords.longitude
    ), (err) -> #console.log err

  pos1 = (getQuery("start") or pos1).split(",")
  pos1 = new google.maps.LatLng(pos1[0],pos1[1])
  pos2 = new google.maps.LatLng(pos2[0],pos2[1])
  done()
setMap = ->
  self.markerA = gmap.addMarker position: pos1, dragend:updRoutes, draggable: true, title: "Inicio", icon: "way_start.png"
  self.markerB = gmap.addMarker position: pos2, dragend:updRoutes, draggable: true, title: "Fin", icon: "way_end.png"
  gmap.fitZoom()
  circleOptions = { map:gmap.map, radius:710, strokeColor:"#999", strokeOpacity:0.7, fillColor:"#add2b9",fillOpacity:0.5, strokeWeight:2 }
  self.circleA = new google.maps.Circle(circleOptions)
  self.circleB = new google.maps.Circle(circleOptions)
  circleA.bindTo("center", markerA, "position")
  circleB.bindTo("center", markerB, "position")

  # Set autocomplete inputs
  completeOpts =
    language:'es'
    componentRestrictions: {country:"pe"}
    bounds: new google.maps.LatLngBounds(
      new google.maps.LatLng(-12.605273,-78.261807),
      new google.maps.LatLng(-11.544393,-76.377652))
  completeStart = new google.maps.places.Autocomplete( document.getElementById('qIni'), completeOpts)
  completeEnd = new google.maps.places.Autocomplete( document.getElementById('qFin'), completeOpts)
  google.maps.event.addListener completeStart, 'place_changed', ->
    place = completeStart.getPlace()
    markerA.setPosition(place.geometry.location)
    updRoutes()
  google.maps.event.addListener completeEnd, 'place_changed', ->
    place = completeEnd.getPlace()
    markerB.setPosition(place.geometry.location)
    updRoutes()
  # Set menu contextual
  setMenu()
setMenu = ->
  contextMenuOptions =
    classNames: menu: "context_menu", menuSeparator: "context_menu_separator"
    menuItems: [
      className: "context_menu_item", eventName: "setMarkerA", label: "Punto de inicio"
    , className: "context_menu_item", eventName: "setMarkerB", label: "Punto de llegada"
    , {}
    , className: "context_menu_item", eventName: "addFav", label: "Guardar esta ubicación"
    ]
  self.contextMenu = new ContextMenu(gmap.map, contextMenuOptions)
  google.maps.event.addListener gmap.map, "rightclick", (e) ->
    contextMenu.show e.latLng
  google.maps.event.addListener contextMenu, "menu_item_selected", (pos, eventName) ->
    map = gmap.map
    switch eventName
      when "setMarkerA" then markerA.setPosition pos; updRoutes()
      when "setMarkerB" then markerB.setPosition pos; updRoutes()
      when "addFav"
        bootbox.prompt "Un nombre para reconocerla", (result) ->
          if result
            Usuario.current.addFavorito lat: pos.lat(), lng: pos.lng(), name: result
getRoutes = ->
  query = [
    circleA.getBounds().toUrlValue(5)
  , circleB.getBounds().toUrlValue(5)
  , markerA.position.toUrlValue().split(",").reverse().join(" ")
  , markerB.position.toUrlValue().split(",").reverse().join(" ")
  ]
  console.time("get routes")
  Meteor.apply "listRutas", query, (err, res) ->
    console.timeEnd("get routes")
    if err then console.error(err)
    else
      locRuta.remove({})
      _.each res.rows, (r) -> locRuta.create(r)
showRecorrido = (err, res) ->
  if err then alert err
  else
    if res.error
      console.error res.error
      return false
    updMap()
    res.rutaId = Session.get("rutaClicked")
    Detalle.create res
    res.steps = []
    gmap.getRoutes
      origin: res.puntoa
      destination: res.puntob
      waypoints: [
        location: new google.maps.LatLng(res.stopa[0], res.stopa[1]), stopover:true
      , location: new google.maps.LatLng(res.stopb[0], res.stopb[1]), stopover:true ]
      travelMode: 'walking'
      callback: (response) ->
        [w1,wt,w2] = response[0].legs
        w1.path = _.flatten _.pluck(w1.steps,"path")
        _.first(w1.steps).img = "transit-walk"
        _(w1.steps).each (s) -> res.steps.push d:s.distance.text, t:s.instructions, i:s.img
        gmap.drawPolyline path:w1.path, strokeColor: "#E83D55", strokeWeight: 7

        cors = ["#1FB25A", "#d17560"]
        _.each res.path, (p, i) ->
          texto = unless i then """Toma el <b>bus</b> con ruta: <a href="#">#{res.ruta_name or res.rna}</a>"""
          else """Bajar y tomar la ruta <a href="#">#{res.rnb}</a>"""
          img = unless i then "bus-stop" else "connection"
          res.steps.push t:texto, i:"transit-"+img

          pathBus = google.maps.geometry.encoding.decodePath(p)
          pathBus = _.map pathBus, (p) -> new google.maps.LatLng p.lng(), p.lat()
          gmap.drawPolyline path:pathBus, strokeColor: cors[i], strokeWeight: 7

        w2.path = _.flatten _.pluck(w2.steps,"path")
        _.first(w2.steps).img = "transit-walk"
        _.last(w2.steps).img = "glyphicon glyphicon-flag"
        _(w2.steps).each (s) -> res.steps.push d:s.distance.text, t:s.instructions, i:s.img
        gmap.drawPolyline path:w2.path, strokeColor: "#E83D55", strokeWeight: 7
        Detalle.update {rutaId: Session.get("rutaClicked")}, $set:
          steps: res.steps
          mins: (w1.duration.value+w2.duration.value+res.duration)/60
          kms: (w1.distance.value+w2.distance.value+res.distance)/1000

        # Indicar donde bajar y tomar nueva ruta en caso transbordo
        if res.path.length > 1
          pos = w1.steps.length+1
          path = gmap.polylines[2].getPath()
          path = path.getAt(0)

          GMaps.geocode
            lat: path.lat()
            lng: path.lng()
            callback: (geoRes, geoStatus) ->
              if geoStatus is "OK"
                t = geoRes[0].formatted_address.replace(/,.*/,"")
                _tmp = {}
                _tmp["steps.#{pos}.t"] = """Bajar en #{t} y tomar la ruta <a href="#">#{res.rnb}</a>"""
                Detalle.update {rutaId: Session.get("rutaClicked")}, $set: _tmp

updMap = ->
  gmap.cleanRoute()
  gmap.removeRoutes()
  gmap.fitZoom()
updRoutes = ->
  Detalle.remove({})
  updMap()
  getRoutes()
