mapLoaded = false

new Controller.Presenter "ruta",
  index:
    rutas: -> Ruta.find()
  edit:
    rendered: ->
      unless mapLoaded
        initialize()
        initEditor()
        mapLoaded = true
    destroyed: -> mapLoaded = false
    ruta: -> Ruta.findOne Router.data.rutaId
    events:
      "click #save": (e, tpl) ->
        e.preventDefault()
        data = $("form").toObject()
        self = this
        data.ruta = poly?.getPath().getArray().toString()
        @update_attributes data
        img = document.getElementById("img").files[0]
        if img
          smartFile.upload(img, {path: "vehiculos", fileName: "#{data.nombre}.png" }, (err, res) ->
            if err then console.log(err)
            else
              self.update_attribute "img", smartFile.publicRootUrl+res.path
          )
        Router.to("root")
  new:
    rendered: ->
      unless mapLoaded
        initialize()
        initEditor()
        mapLoaded = true
    destroyed: -> mapLoaded = false
    events:
      "click #save": (e, tpl) ->
        e.preventDefault()
        data = $("form").toObject()
        data.ruta = poly?.getPath().getArray().map( (x) -> x.lng()+" "+x.lat() ).join ","
        rutaId = Usuario.current.addRuta data
        img = document.getElementById("img").files[0]
        if img
          smartFile.upload(img, {path: "vehiculos", fileName: "#{data.nombre}.png" }, (err, res) ->
            if err then console.log(err)
            else
              Ruta.update rutaId, $set: img: smartFile.publicRootUrl+res.path
          )
        Router.to("root")