style = [{"featureType":"water","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]},{"featureType":"landscape","stylers":[{"color":"#f2e5d4"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"administrative","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"road"},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{},{"featureType":"road","stylers":[{"lightness":20}]}]
self = this
self.initialize = ->
  self.gmap = new GMaps
    div: "#map-canvas"
    lat: -12.096479
    lng: -77.041025
    zoom: 12
    disableDefaultUI: true
    styles: style
  extendGoogle()
self.initEditor = ->
  self.drawingManager = new google.maps.drawing.DrawingManager
    drawingMode: google.maps.drawing.OverlayType.POLYLINE
    drawingControl: false
    drawingControlOptions:
      drawingModes: [ google.maps.drawing.OverlayType.POLYLINE ]
    polylineOptions:
      editable: true
  drawingManager.setMap self.gmap.map
  google.maps.event.addListener drawingManager, "polylinecomplete", (poly) ->
    drawingManager.setDrawingMode null
    self.poly = poly
    google.maps.event.addListener self.poly, "click", deleteNode

deleteNode = (mev) ->
  @getPath().removeAt mev.vertex  if mev.vertex? and @getPath().getLength() > 2

self.getQuery = (variable) ->
  vars = window.location.search.slice(1).split("&")
  for par in vars
    [k,v] = par.split "="
    return decodeURIComponent v if k is variable
  return false

extendGoogle = ->
  google.maps.Polyline.prototype.getBounds = ->
    bounds = new google.maps.LatLngBounds()
    @getPath().forEach (e) ->
      bounds.extend(e)
    return bounds
