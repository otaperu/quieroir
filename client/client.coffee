helpers =
  extend: (partial, options) ->
    output = null
    context = Object.create(this)
    template = Template[partial]
    throw new Meteor.Error("Missing layout partial: '" + partial + "'")  if typeof template is "undefined"
    options.fn context
    Template[partial] context
  append: (block, options) ->
    @blocks = @blocks or {}
    @blocks[block] =
      should: "append"
      fn: options.fn
  prepend: (block, options) ->
    @blocks = @blocks or {}
    @blocks[block] =
      should: "prepend"
      fn: options.fn
  replace: (block, options) ->
    @blocks = @blocks or {}
    @blocks[block] =
      should: "replace"
      fn: options.fn
  block: (name, options) ->
    block = null
    @blocks = @blocks or {}
    block = @blocks[name]
    switch block and block.fn and block.should
      when "append" then options.fn(this) + block.fn(this)
      when "prepend" then block.fn(this) + options.fn(this)
      when "replace" then block.fn this
      else options.fn this
  ifwith: (ctx, opts) -> if ctx then opts.fn(ctx) else opts.inverse(@)
  sessionEquals: (ses, val) -> Session.equals ses, val
  sessionGet: (ses) -> Session.get ses
  viewGet: (view) -> View.get(view)
  getUsername: (id) -> Usuario.findOne(id)?.username or "anónimo"
  fromNow: (fecha) -> moment(fecha).fromNow() or ""
  smartPic: (img) -> Meteor.settings.public.smart.cdnUrl+img+".jpg" if img
_.each helpers, (fn, name) -> Handlebars.registerHelper name, fn

Meteor.subscribe "favs"
Meteor.subscribe "rutas"
Meteor.subscribe "empresas"
Meteor.subscribe "notas"

Router.config
  map:
    ruta:
      edit: (id) ->
        Router.render title: "Editar Ruta", rutaId: id

Router.root "app#index"
Router.get "/rutas/admin", "ruta#index"
Router.get "/ruta/:id/edit", "ruta#edit", to: "rutaEdit"
Router.get "/ruta/new"
