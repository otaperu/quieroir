class @Usuario extends BasicModel
  @collection Meteor.users
  @vprop "current", -> Meteor.user()
  @vprop "currentId", -> Meteor.userId()
  @hasMany "Favorito", false, "by"
  @hasMany "Nota", false, "by"
  @hasMany "Ruta", false, "by"
class @Favorito extends BasicModel
  @collection "favoritos"
  @belongsTo "Usuario", false, "by"
  @authorize:
    insert: (userId, doc) -> userId is doc.by
    update: (userId, doc) -> userId is doc.by
    remove: (userId, doc) -> userId is doc.by
class @Ruta extends BasicModel
  @collection "rutas"
  @belongsTo "Empresa", false, "op"
  @hasMany "Nota", false, "for"
  @authorize:
    insert: (userId, doc) -> userId is doc.by
    update: (userId, doc) -> userId is doc.by
    remove: (userId, doc) -> userId is doc.by
class @Empresa extends BasicModel
  @collection "empresas"
  # @hasMany "Ruta", false, "op"
  @authorize:
    insert: -> true
    update: -> true
    remove: -> false
class @Nota extends BasicModel
  @collection "notas"
  @belongsTo "Ruta", false, "for"
  @belongsTo "Usuario", false, "by"
  @authorize:
    insert: (userId, doc) -> userId is doc.by
    update: (userId, doc) -> userId is doc.by
    remove: -> false
