@smartFile = new SmartFile
  key: Meteor.settings.smart.key
  password: Meteor.settings.smart.password
  basePath: Meteor.settings.smart.basePath
  publicRootUrl: Meteor.settings.public.smart.publicRootUrl
