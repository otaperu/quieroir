Meteor.publish "favs", ->
  Favorito.find by: @userId
Meteor.publish "rutas", ->
  Ruta.find()
Meteor.publish "empresas", ->
  Empresa.find()
Meteor.publish "notas", ->
  Nota.find()
