pg = Meteor.require("pg")
path = Meteor.require("path")
conString = Meteor.settings.dbUrl
client = new pg.Client(conString)
client.connect()
srid = 4326
json = status: 400, message: "No hay data"

listRutas = (marka, markb, puntoa, puntob, callback) ->
  if marka
    marka = marka.split(",")
    markb = markb.split(",")
    marka = "ST_MakeEnvelope(" + marka[1] + "," + marka[0] + "," + marka[3] + "," + marka[2] + "," + srid + ")"
    markb = "ST_MakeEnvelope(" + markb[1] + "," + markb[0] + "," + markb[3] + "," + markb[2] + "," + srid + ")"
    tmppuntoa = "ST_GeomFromText('POINT(#{puntoa})',#{srid})"
    tmppuntob = "ST_GeomFromText('POINT(#{puntob})',#{srid})"
    str = "SELECT * FROM listrutas(#{tmppuntoa}, #{tmppuntob}, #{marka}, #{markb})"
    client.query(str).on("row", (row, result) ->
      result.addRow row
    ).on "end", (result) ->
      if result.rows.length < 1
        listTransbordo marka, markb, puntoa, puntob, (err, res) ->
          callback err, res
      else callback null, { rows: result.rows, type: "simple" }
  else
    callback "Faltan parametros"
showRuta = (rutaid, puntoa, puntob, callback) ->
  if rutaid
    lineA = undefined
    lineB = undefined
    tmppuntoa = "ST_GeomFromText('POINT(#{puntoa})',#{srid})"
    tmppuntob = "ST_GeomFromText('POINT(#{puntob})',#{srid})"
    client.query "SELECT * FROM showruta(#{tmppuntoa}, #{tmppuntob}, #{rutaid})", (err, result) ->
      if result.rows[0].linea > result.rows[0].lineb
        lineA = result.rows[0].linea
        lineB = result.rows[0].lineb
      else
        lineB = result.rows[0].linea
        lineA = result.rows[0].lineb
      string = "SELECT * FROM showrutadetalle(#{tmppuntoa}, #{tmppuntob}, #{rutaid})"
      client.query string, (err, result) ->
        if result.rows.length < 1
          json = error: "No existe la ruta indicada."
          callback null, json
        else
          _.extend result.rows[0],
            puntoa: JSON.parse(result.rows[0].puntoa).coordinates.reverse()
            puntob: JSON.parse(result.rows[0].puntob).coordinates.reverse()
            stopa: JSON.parse(result.rows[0].stopa).coordinates.reverse()
            stopb: JSON.parse(result.rows[0].stopb).coordinates.reverse()
          rquery = "SELECT * FROM showrutapaths(#{lineA}, #{lineB}, #{result.rows[0].id})"
          client.query rquery, (err, resgeo) ->
            resgeo = _.extend resgeo.rows[0], result.rows[0]
            resgeo.path = [ createEncodings(JSON.parse(resgeo.path).coordinates) ]
            callback null, resgeo
  else
    callback null, error: "Faltan parametros"
showTransbordo = (ruta1, ruta2, puntoa, puntob, callback) ->
  if ruta2
    tmppuntoa = "ST_GeomFromText('POINT(#{puntoa})',#{srid})"
    tmppuntob = "ST_GeomFromText('POINT(#{puntob})',#{srid})"
    string = "SELECT * FROM showtransbordo(#{tmppuntoa}, #{tmppuntob}, #{ruta1}, #{ruta2}"
    client.query string, (err, result) ->
      fila = result.rows[0]
      if fila.a1 <= fila.a0
        amin = fila.a1
        amax = fila.a0
      else
        amin = fila.a0
        amax = fila.a1
      if fila.b1 <= fila.b0
        bmin = fila.b1
        bmax = fila.b0
      else
        bmin = fila.b0
        bmax = fila.b1
      string = "SELECT * FROM showtransbordopaths(#{amin}, #{amax}, #{bmin}, #{bmax}, #{ruta1}, #{ruta2}"
      client.query string, (err, result) ->
        if err then callback err
        if result.rows.length < 1
          json = error:"No existe la ruta indicada."
          callback null, json
        else
          resgeo = result.rows[0]
          resgeo.puntoa = puntoa.split(" ").reverse()
          resgeo.puntob = puntob.split(" ").reverse()
          resgeo.stopa = JSON.parse(fila.orga).coordinates.reverse()
          resgeo.stopb = JSON.parse(fila.orgb).coordinates.reverse()
          xpathx = JSON.parse(resgeo.path).coordinates
          resgeo.path = [ createEncodings(xpathx[0]), createEncodings(xpathx[1]) ]
          resgeo.duration = resgeo.distance / 3.8
          callback null, resgeo
  else
    callback null, error: "Faltan parametros"
listTransbordo = (marka, markb, puntoa, puntob, callback) ->
  tmppuntoa = "ST_GeomFromText('POINT(#{puntoa})',#{srid})"
  tmppuntob = "ST_GeomFromText('POINT(#{puntob})',#{srid})"

  string = "SELECT * FROM listtransbordo(#{tmppuntoa}, #{tmppuntob}, #{marka}, #{markb}"
  client.query string, (err, results) ->
    if err then callback err
    else if results.rows.length < 1
      json = error: "No existe la ruta indicada."
      callback null, json
    else
      callback null, { rows: results.rows, type: "transbordo" }

createEncodings = (coords) ->
  i = 0
  plat = 0
  plng = 0
  encoded_points = ""
  i = 0
  while i < coords.length
    lat = coords[i][0]
    lng = coords[i][1]
    encoded_points += encodePoint(plat, plng, lat, lng)
    plat = lat
    plng = lng
    ++i
  return encoded_points
encodePoint = (plat, plng, lat, lng) ->
  late5 = Math.round(lat * 1e5)
  plate5 = Math.round(plat * 1e5)
  lnge5 = Math.round(lng * 1e5)
  plnge5 = Math.round(plng * 1e5)
  dlng = lnge5 - plnge5
  dlat = late5 - plate5
  return encodeSignedNumber(dlat) + encodeSignedNumber(dlng)
encodeSignedNumber = (num) ->
  sgn_num = num << 1
  sgn_num = ~(sgn_num)  if num < 0
  return encodeNumber sgn_num
encodeNumber = (num) ->
  encodeString = ""
  while num >= 0x20
    encodeString += (String.fromCharCode((0x20 | (num & 0x1f)) + 63))
    num >>= 5
  encodeString += (String.fromCharCode(num + 63))
  return encodeString

Meteor.methods
  listRutas: (marka, markb, puntoa, puntob) ->
    listRutasSync = Meteor._wrapAsync listRutas
    return listRutasSync(marka, markb, puntoa, puntob)
  showRuta: (rutaid, puntoa, puntob) ->
    showRutaSync = Meteor._wrapAsync showRuta
    return showRutaSync(rutaid, puntoa, puntob)
  showTransbordo: (ruta1, ruta2, puntoa, puntob) ->
    showTransbordoSync = Meteor._wrapAsync showTransbordo
    return showTransbordoSync(ruta1, ruta2, puntoa, puntob)
